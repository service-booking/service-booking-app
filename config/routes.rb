Rails.application.routes.draw do
  resources :users
  root 'static_page#home'
  get 'static_page/home'
  get 'sessions/new'
  get '/signup', to: 'users#new'
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  get '/logout', to: 'sessions#destroy'

end
